#projet ossature partie IoT
#client MQTT sur la base d'un ESP32
#simule un capteur de température sur l'entrée analogique 33
#simule un voyant sur la sortie 22 (weemos)
#Appuyer sur le bouton reset pour lancer le programme boot.py


def connectAP():
    import network
    # wifi en Access Point
    ap = network.WLAN(network.AP_IF)
    ap.active(True)
    # ap.config(essid='ap',authmode=3,password='password')
    ap.config(essid='ap')
    # ap.ifconfig(('192.168.4.123', '255.255.255.0', '192.168.0.1', '8.8.8.8'))
    uri = "http://"+ap.ifconfig()[0]+":80"
    print('AP accessible:   ', uri)
    #print(ap.status('stations'))
    #(ip, subnet, gateway, dns) = ap.ifconfig()
    #print("Access Point:\n\tip: {}\n\tsubnet: {}\n\tgateway: {}\n\tdns: {}".format(ip, subnet, gateway, dns))

def connectAF():
    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    #wlan.scan()             # scan for access points
    if not wlan.isconnected():
        ############################ adapter ########################################
        #wlan.connect('tonSSID', 'tonMDP') # connect to an AccesPoint extern
        wlan.connect('ossature24g', 'ossature24g') # connect to an AccesPoint extern
        #############################################################################
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())
    
    
def connectWR(): #webrepl access
    
    def do_connect(ssid, pwd):
        import network
        sta_if = network.WLAN(network.STA_IF)
        if not sta_if.isconnected():
            print('connecting to network...')
            sta_if.active(True)
            sta_if.connect(ssid, pwd)
            while not sta_if.isconnected():
                pass
        print('network config:', sta_if.ifconfig())
 
    # This file is executed on every boot (including wake-boot from deepsleep)
    #import esp
    #esp.osdebug(None)
 
    # Attempt to connect to WiFi network
    do_connect('ossature24g', 'ossature24g')
 
    import webrepl 
    #import webrepl_setup
    #webrepl.start()
    # or, start with a specific password 4-9 caracteres
    webrepl.start(password='aaaa')

    
#connectAP() #faites votre choix
#connectAF()
connectWR()
