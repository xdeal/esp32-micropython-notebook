from machine import I2C, Pin

rst = Pin(16, Pin.OUT)
rst.value(1)
i2c = I2C(scl=Pin(22, Pin.OUT, Pin.PULL_UP), sda=Pin(21, Pin.OUT, Pin.PULL_UP), freq=450000)
assert 0x3c in i2c.scan(), "OLED i2c not found"

import framebuf

# There is an extra byte to the data buffer to hold an I2C data/command byte
# to use hardware-compatible I2C transactions.
buffer = bytearray(((64 // 8) * 128) + 1)
buffer[0] = 0x40  # Set first byte of data buffer to Co=0, D/C=1
fbuff = framebuf.FrameBuffer1(memoryview(buffer)[1:], 128, 64)

cmdforinit = bytes((0xae,        # CMD_DISP=off
                    0x20, 0x00,  # SET_MEM_ADDR  horizontal
                    0x40,        # SET_DISP_START_LINE
                    0xa0 | 0x01, # column addr 127 mapped to SEG0
                    0xa8, 63,    # SET_MUX_RATIO, height-1
                    0xc0 | 0x08, # SET_COM_OUT_DIR scan from COM[N] to COM0
                    0xd3, 0x00,  # SET_DISP_OFFSET
                    0xda, 0x12,  # SET_COM_PIN_CFG
                    0xd5, 0x80,  # SET_DISP_CLK_DIV
                    0xd9, 0xf1,  # SET_PRECHARGE
                    0xdb, 0x30,  # SET_VCOM_DESEL 0.83*Vcc
                    0x81, 0xff,  # SET_CONTRAST maximum
                    0xa4,        # SET_ENTIRE_ON output follows RAM contents
                    0xa6,        # SET_NORM_INV not inverted
                    0x8d, 0x14,  # SET_CHARGE_PUMP
                    0xae | 0x01  # SET_DISP on
                   ))
for c in cmdforinit:
    i2c.writeto(0x3c, bytes((0x80, c)))

cmdforshow = bytes((0x80, 0x21, # SET_COL_ADDR
                    0x80, 0,    # 0
                    0x80, 127,  # width-1
                    0x80, 0x22, # SET_PAGE_ADDR
                    0x80, 0,    # 0
                    0x80, 7     # height//8 - 1
                    ))   
 
def oledshow():
    i2c.writeto(0x3c, cmdforshow)
    i2c.writeto(0x3c, buffer)
    
def oledcontrast(contrast):
    i2c.writeto(0x3c, bytes((0x80, 0x81, 0x80, contrast)))

def oledinvert(invert=True):
    i2c.writeto(0x3c, bytes((0x80, 0xa6 | (invert & 1))))

# checkerboard starting page
for i in range(0, 128, 8):
    for j in range(0, 64, 8):
        fbuff.fill_rect(i, j, 8, 8, (i//8 + j//8)%2)
oledshow()